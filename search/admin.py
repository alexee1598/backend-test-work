from django.contrib import admin

# Register your models here.
from search.models import Location, Task, SearchResult, SearchEngine


@admin.register(Location)
class LocationAdmin(admin.ModelAdmin):
    list_display = ('location_code', 'language_code', 'location_name',)


@admin.register(Task)
class TaskAdmin(admin.ModelAdmin):
    list_display = ('id', 'status_code', 'keyword', 'status_message', 'cost', 'status', 'created', 'completed')


@admin.register(SearchResult)
class SearchResultAdmin(admin.ModelAdmin):
    list_display = ('id', 'title', 'type', 'domain', 'task',)
    list_filter = ("task__id",)


@admin.register(SearchEngine)
class SearchResultAdmin(admin.ModelAdmin):
    list_display = ('id', 'name',)
