from rest_framework import serializers

from search.models import Location, Task, SearchEngine, SearchResult


class LocationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Location
        fields = '__all__'


class SearchEngineSerializer(serializers.ModelSerializer):
    class Meta:
        model = SearchEngine
        fields = '__all__'


class SearchResultSerializer(serializers.ModelSerializer):
    class Meta:
        model = SearchResult
        fields = ('title', 'description', 'url')


class TaskSerializer(serializers.ModelSerializer):
    search_result = SearchResultSerializer(many=True, read_only=True)

    class Meta:
        model = Task
        fields = ('id', 'status', 'created', 'completed', 'keyword', 'search_result')
