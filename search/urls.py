from django.urls import path

from search import views

urlpatterns = [
    path('', views.search_data_view, name='index-view'),
    path('results/', views.get_new_search_result_view, name='index-view'),
]
