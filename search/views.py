import datetime

from rest_framework.decorators import api_view
from rest_framework.response import Response

from search.api.client import RestClient
from search.api.endpoint_regular import get_all_result_regular
from search.models import Task, SearchResult, Location, SearchEngine
from search.serializers import LocationSerializer, SearchEngineSerializer, TaskSerializer

client = RestClient("alexee151@gmail.com", "997ab57957f7c9de")


@api_view(['GET', 'POST'])
def search_data_view(request) -> Response:
    if request.method == 'GET':
        location = Location.objects.all()
        search_engine = SearchEngine.objects.all()
        return Response({
            'locations': LocationSerializer(location, many=True).data,
            'engines': SearchEngineSerializer(search_engine, many=True).data,
            'result': 1,
        })
    else:
        try:
            post_data = dict()
            search_engine = str(request.data.get('engine').get('name')).lower()
            location_code = request.data.get('location').get('location_code')
            request_keyword = request.data.get('request')

            post_data[len(post_data)] = dict(
                language_code='en',
                location_code=location_code,
                priority=1,
                keyword=request_keyword
            )
            response = client.post(f'/v3/serp/{search_engine}/organic/task_post', data=post_data)
            task = response.get('tasks')[-1]
            t = Task(cost=task.get('cost'),
                     status_code=task.get('status_code'),
                     id=task.get('id'),
                     keyword=task.get('data').get('keyword'),
                     status_message=task.get('status_message'))
            t.save()
        except Exception as e:
            return Response({
                'error': e,
                'result': 0,
            })
        else:
            return Response({
                'error': '',
                'result': 1,
            })


@api_view(['GET', 'POST'])
def get_new_search_result_view(request) -> Response:
    if request.method == 'GET':
        tasks = Task.objects.all().order_by('-created')
        return Response({
            'tasks': TaskSerializer(tasks, many=True).data,
            'result': 1,
        })
    if request.method == 'POST':
        try:
            for s in list(SearchEngine.objects.all()):
                response = client.get(f'/v3/serp/{s.name.lower()}/organic/tasks_ready')
                if response["status_code"] == 20000:
                    results = get_all_result_regular(response=response, client=client)
                    if len(results) > 0:
                        _save_results(results)
            return Response({
                'result': 1,
            })
        except Exception as e:
            return Response({
                'result': 0,
                'error': e,
            })


def _save_results(results) -> None:
    search_items_result = []
    try:
        for r in results:
            if (task := Task.objects.filter(pk=r['tasks'][0]['id']).first()) is not None:
                result = r['tasks'][0]['result'][0]

                task.status = task.COMPLETED
                task.completed = datetime.datetime.strptime(result['datetime'][0:-7], '%Y-%m-%d %H:%M:%S')
                task.save()
                if (items := result.get('items', None)) is not None:
                    for res in items:
                        data = {'task': task,
                                'type': res.get('type', ''),
                                'title': res.get('title', 'No title'),
                                'description': res.get('description', 'No description'),
                                'domain': res.get('domain', ''),
                                'url': res.get('url', ''),
                                }
                        search_items_result.append(SearchResult(**data))
        SearchResult.objects.bulk_create(search_items_result)
    except Exception as e:
        raise e
