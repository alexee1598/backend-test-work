from typing import List

from search.api.client import RestClient


def get_all_result_regular(response, client: RestClient) -> List:
    results = []
    for task in response['tasks']:
        if task['result'] and (len(task['result']) > 0):
            for resultTaskInfo in task['result']:
                if resultTaskInfo['endpoint_regular']:
                    results.append(client.get(resultTaskInfo['endpoint_regular']))
    return results
