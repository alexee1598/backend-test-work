from django.db import models


class Location(models.Model):
    location_code = models.IntegerField(unique=True)
    language_code = models.SlugField()
    location_name = models.TextField()


class SearchEngine(models.Model):
    name = models.SlugField()


class Task(models.Model):
    COMPLETED = 'COMPLETED'
    IN_PROCESS = 'IN PROCESS'
    ERROR = 'ERROR'

    STATUS_TASK = (
        (COMPLETED, COMPLETED),
        (IN_PROCESS, IN_PROCESS),
        (ERROR, ERROR),
    )

    id = models.SlugField(primary_key=True, unique=True)
    status_code = models.SmallIntegerField()
    status_message = models.TextField()
    cost = models.FloatField()
    status = models.CharField(max_length=40, choices=STATUS_TASK, default=IN_PROCESS)
    keyword = models.TextField()
    created = models.DateTimeField(auto_now_add=True)
    completed = models.DateTimeField(null=True, blank=True)


class SearchResult(models.Model):
    PAID = 'paid'
    ORGANIC = 'organic'

    TYPE = (
        (PAID, PAID),
        (ORGANIC, ORGANIC),
    )

    type = models.CharField(choices=TYPE, default=ORGANIC, max_length=20)
    domain = models.URLField(max_length=200)
    title = models.TextField()
    description = models.TextField(null=True, blank=True)
    url = models.URLField(max_length=2000)
    task = models.ForeignKey(to=Task, on_delete=models.CASCADE, related_name='search_result', blank=True, null=True)
